define([
	'mod_devpage/config',
	'core/config',
	'core/ajax',
	'cm',
	'cm/mode/htmlmixed/htmlmixed',
	'cm/addon/edit/closebrackets',
	'cm/addon/edit/matchbrackets',
	'cm/addon/edit/matchtags',
	'cm/addon/edit/closetag',
	'cm/addon/fold/foldcode',
	'cm/addon/fold/foldgutter',
	'cm/addon/fold/brace-fold',
	'cm/addon/fold/xml-fold',
	'cm/addon/display/fullscreen'
], function(_, config, ajax, CodeMirror) {
	return {
		init: function(editorId, theme){
			var editor = CodeMirror.fromTextArea(document.getElementById(editorId), {
				theme: theme,
				mode: {
					name: 'htmlmixed',
					tags: {
						style: [
							['type', /.*?scss$/, 'text/x-scss'],
							[null, null, 'css']
						]
					},
				},
				tabSize: 2,
				lineNumbers: true,
				autoCloseBrackets: true,
				matchBrackets: true,
				autoCloseTags: true,
				matchTags: {
					bothTags: true
				},
				foldGutter: true,
				gutters: ['CodeMirror-linenumbers', 'CodeMirror-foldgutter'],
				extraKeys: {
					// toggle fullscreen
					"Shift-LeftDoubleClick": function(cm) {
						if (cm.getOption("fullScreen")) {
							document.querySelector('nav.navbar').style.display = ''
							document.querySelector('#nav-drawer').style.display = ''
							cm.setOption("fullScreen", false)
						} else {
							document.querySelector('nav.navbar').style.display = 'none'
							document.querySelector('#nav-drawer').style.display = 'none'
							cm.setOption("fullScreen", true)
						}
					},
					// escape full screen
					"Esc": function(cm) {
						if (cm.getOption("fullScreen")) {
							cm.setOption("fullScreen", false)
							document.querySelector('nav.navbar').style.display = ''
							document.querySelector('#nav-drawer').style.display = ''
						}
					},
					// save editor content when updating
					"Shift-Space": function(cm) {
						var cmId = window.location.href.match(/update=(\d+)/)
						cmId = cmId ? parseInt(cmId[1]) : 0
						if (cmId == 0) return

						var courseId = document.body.className.match(/course-(\d+)/)
						courseId = courseId ? parseInt(courseId[1]) : 0

						ajax.call([{
							methodname: 'mod_devpage_set_devpage_content',
							args: {
								courseid: courseId,
								cmid: cmId,
								content: cm.getValue()
							},
							done: function(response) { console.log(response ? 'Saved content' : 'Failed to save content') },
							fail: function(error) { console.log(error) }
						}])
					}
				}
			})
			editor.setSize('100%', '40rem')
		}
	}
})
