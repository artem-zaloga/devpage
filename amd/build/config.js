define([], function () {
	window.require.config({
	  packages: [{
	    name: 'cm',
	    location: M.cfg.wwwroot + '/mod/devpage/js/codemirror',
	    main: 'lib/codemirror'
	  }]
	})
})
