// Moodle throws errors in the form of an object:
// { backtrace, debuginfo, errorcode, link, message, moreinfourl }

define(['core/config'], function(config) {

	// helpers
	const cmId = url => parseInt((url.match(/id=(\d+)/) || [null, '0'])[1])
	const courseId = () => parseInt((document.body.className.match(/course-(\d+)/) || [null, '0'])[1])

	const callbackPromise = (error, data) => error ? Promise.reject(error) : Promise.resolve(data)

	// instead of ajax.call
	const call = (fn, data = {}, callback = callbackPromise) => 
		fetch(config.wwwroot + '/mod/devpage/service.php?sesskey=' + config.sesskey, {
			method: 'POST',
			body: JSON.stringify([{ methodname: fn, args: data }])
		})
			.then(r => r.json())
			.then(r => {
				if (r[0].error) return callback(r[0])

				let response = r[0].data

				// transform devpage webservice json data
				if (fn.startsWith('mod_devpage')) {
					response = JSON.parse(response)

					if (fn == 'mod_devpage_get_devpage_response' && data.all) {
						const allResponses = {}
						for (userId in response) allResponses[userId] = JSON.parse(response[userId])
						response = allResponses
					}
				}

				return callback(undefined, response)
			})


	// api functions
	return {
		// call arbitrary webservice
		call(fnName, data, callback) {
			return call(fnName, data, callback)
		}, 

		// get devpage response
		getResponse(callback, options = {}) {
			// callback is actually options if called api method is called as promise
			if (typeof callback == 'object') {
				options = callback
				callback = undefined
			}

			return call('mod_devpage_get_devpage_response', {
				cmid: cmId(options.url || window.location.href),
				all: false
			}, callback)
		},

		// list all devpage responses
		listResponses(callback, options = {}) {
			if (typeof callback == 'object') {
				options = callback
				callback = undefined
			}

			return call('mod_devpage_get_devpage_response', {
				cmid: cmId(options.url || window.location.href),
				all: true
			}, callback)
		},

		// set devpage response
		setResponse(response, callback, options = {}) {
			if (typeof callback == 'object') {
				options = callback
				callback = undefined
			}

			return call('mod_devpage_set_devpage_response', {
				cmid: cmId(window.location.href),
				userid: options.userId || 0,
				response: JSON.stringify(response),
				complete: !!options.complete
			}, callback)
		},

		// get user in-course data
		getUser(callback) {
			return call('mod_devpage_get_user_data', {
				courseid: courseId(),
				cmid: cmId(window.location.href),
				all: false
			}, callback)
		},

		// get user in-course data
		listUsers(callback) {
			return call('mod_devpage_get_user_data', {
				courseid: courseId(),
				cmid: cmId(window.location.href),
				all: true
			}, callback)
		}
	}
})
