<?php

/**
 * The page that is shown when viewing a devpage instance.
 */

require('../../config.php');
require_once($CFG->dirroot.'/mod/devpage/lib.php');
require_once($CFG->libdir.'/completionlib.php');

// get cmid from url, then cm from cmid, and devpage and course from cm
$cmid = optional_param('id', 0, PARAM_INT);
$cm = get_coursemodule_from_id('devpage', $cmid);
if (!$cm) {
    print_error('invalidcoursemodule');
}
$devpage = $DB->get_record('devpage', ['id'=>$cm->instance], '*', MUST_EXIST);
$course = $DB->get_record('course', ['id'=>$cm->course], '*', MUST_EXIST);

// make sure user is logged in and capable of viewing
require_course_login($course, true, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/devpage:view', $context);

// trigger devpage view event & view page completion (if required)
devpage_view($devpage, $course, $cm, $context);

// set page variables
$PAGE->set_url('/mod/devpage/view.php', ['id' => $cm->id]);
$PAGE->set_title($course->shortname.': '.$devpage->name);
$PAGE->set_heading($course->fullname);
$PAGE->set_activity_record($devpage);

// check if embed=true is in url, and if so use 'embedded' template
$embedded = optional_param('embed', 0, PARAM_BOOL);
if ($embedded) {
  $PAGE->set_pagelayout('embedded');
}

// time to pre-process devpage content
$content = $devpage->content;

// 1. convert <style type="scss">...</style>
$content = preg_replace_callback(
    '/<style type="scss">([\s\S]*?)<\/style>/',
    function ($matches) {
        $compiler = new core_scss();
        $compiler->append_raw_scss($matches[1]);
        return "<style>{$compiler->to_css()}</style>";
    },
    $content
);

// 2. add <script type="module">...</script> to requirejs stack
$modules = [];
$content = preg_replace_callback(
    '/<script type="module">([\s\S]*?)<\/script>/',
    function ($matches) {
        global $modules;
        $modules[] = $matches[1];
        return '';
    },
    $content
);

foreach ($modules as $module) {
    $PAGE->requires->js_amd_inline($module);
};

// show everything
echo $OUTPUT->header();
if ($embedded) {
  echo $OUTPUT->box($content, "generalbox center clearfix embedded");
} else {
  echo $OUTPUT->heading(format_string($devpage->name), 2);
  echo $OUTPUT->box($content, "generalbox center clearfix");
}
echo $OUTPUT->footer();
