<?php

$string['createdevpage'] = 'Create a new devpage resource';

$string['contentheader'] = 'Content';
$string['content'] = 'Raw HTML content';

$string['modulename'] = 'Devpage';
$string['modulename_help'] = 'The devpage module allows you to work like a real frontend dev in Moodle.';
$string['modulename_link'] = 'mod/devpage/view';
$string['modulenameplural'] = 'Devpages';

$string['devpage:view'] = 'View devpage content';
$string['devpage:addinstance'] = 'Add a new devpage resource';
$string['devpage:datafullaccess'] = 'Access and modify all data associated with a devpage instance';


$string['pluginadministration'] = 'Devpage module administration';
$string['pluginname'] = 'Devpage';

$string['printheading'] = 'Display devpage name';
$string['printheadingexplain'] = 'Display devpage name above content?';
$string['printintro'] = 'Display devpage description';
$string['printlastmodified'] = 'Display last modified date';
$string['printlastmodifiedexplain'] = 'Display last modified date below content?';
$string['privacy:metadata'] = 'The Devpage resource plugin does not store any personal data.';
$string['search:activity'] = 'Devpage';

$string['resetresponses'] = 'Delete all user responses';

$string['customcompletion'] = 'Show as complete when setResponse api function sets "complete" to true';
$string['customcompletiondetail'] = 'Complete activity';
