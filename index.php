<?php

/**
 * The page that lists all devpage instances in a course.
 * Mostly stolen from the page module; TODO: fix this shit
 */

require('../../config.php');

$id = required_param('id', PARAM_INT); // course id

$course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);

require_course_login($course, true);
$PAGE->set_pagelayout('incourse');

$strpage         = get_string('modulename', 'devpage');
$strpages        = get_string('modulenameplural', 'devpage');
$strname         = get_string('name');
$strintro        = get_string('moduleintro');
$strlastmodified = get_string('lastmodified');

$PAGE->set_url('/mod/devpage/index.php', array('id' => $course->id));
$PAGE->set_title($course->shortname.': '.$strpages);
$PAGE->set_heading($course->fullname);
$PAGE->navbar->add($strpages);
echo $OUTPUT->header();
echo $OUTPUT->heading($strpages);
if (!$devpages = get_all_instances_in_course('devpage', $course)) {
    notice(get_string('thereareno', 'moodle', $strpages), "$CFG->wwwroot/course/view.php?id=$course->id");
    exit;
}

$usesections = course_format_uses_sections($course->format);

$table = new html_table();
$table->attributes['class'] = 'generaltable mod_index';

if ($usesections) {
    $strsectionname = get_string('sectionname', 'format_'.$course->format);
    $table->head  = [$strsectionname, $strname, $strintro];
    $table->align = ['center', 'left', 'left'];
} else {
    $table->head  = [$strlastmodified, $strname, $strintro];
    $table->align = ['left', 'left', 'left'];
}

$modinfo = get_fast_modinfo($course);
$currentsection = '';
foreach ($devpages as $devpage) {
    $cm = $modinfo->cms[$devpage->coursemodule];
    if ($usesections) {
        $printsection = '';
        if ($devpage->section !== $currentsection) {
            if ($devpage->section) {
                $printsection = get_section_name($course, $devpage->section);
            }
            if ($currentsection !== '') {
                $table->data[] = 'hr';
            }
            $currentsection = $devpage->section;
        }
    } else {
        $printsection = '<span class="smallinfo">'.userdate($devpage->timemodified)."</span>";
    }

    $class = $devpage->visible ? '' : 'class="dimmed"'; // hidden modules are dimmed

    $table->data[] = array (
        $printsection,
        "<a $class href=\"view.php?id=$cm->id\">".format_string($devpage->name)."</a>",
        format_module_intro('devpage', $devpage, $cm->id));
}

echo html_writer::table($table);

echo $OUTPUT->footer();
