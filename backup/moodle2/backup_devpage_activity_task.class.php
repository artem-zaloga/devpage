<?php

/**
 * Backup individual devpage instances
 * by calling the backup steps (./backup_devpage_stepslib.php)
 * and encoding devpage urls.
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot . '/mod/devpage/backup/moodle2/backup_devpage_stepslib.php');

class backup_devpage_activity_task extends backup_activity_task
{
    // no settings, but still need this
    protected function define_my_settings() {}


    // backup steps to store the instance data in devpage.xml backup file
    protected function define_my_steps() {
        $this->add_step(new backup_devpage_activity_structure_step('devpage_structure', 'devpage.xml'));
    }


    // encode URLs to the devpage index.php and view.php pages so they work in restored course
    public static function encode_content_links($content) {
        global $CFG;

        $base = preg_quote($CFG->wwwroot, "/");

        $content= preg_replace("/(".$base."\/mod\/devpage\/index.php\?id\=)([0-9]+)/", '$@DEVPAGEINDEX*$2@$', $content);
        $content= preg_replace("/(".$base."\/mod\/devpage\/view.php\?id\=)([0-9]+)/", '$@DEVPAGEVIEWBYID*$2@$', $content);

        return $content;
    }
}
