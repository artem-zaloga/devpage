<?php

/**
 * Define the backup steps
 * used by backup_devpage_activity_task (./backup_devpage_activity_task.class.php)
 */

defined('MOODLE_INTERNAL') || die;

class backup_devpage_activity_structure_step extends backup_activity_structure_step {

    // define the tree structure: devpage > devpage_responses
    protected function define_structure() {

        // define main tree node and source table
        $devpage = new backup_nested_element('devpage', ['id'], ['name', 'intro', 'introformat', 'content', 'revision', 'timemodified', 'customcompletion']);
        $devpage->set_source_table('devpage', ['id' => backup::VAR_ACTIVITYID]);

        // define file annotation areas (can't add files in content, only intro)
        $devpage->annotate_files('mod_devpage', 'intro', null);

        // need child nodes only if userdata is included in backup
        if ($this->get_setting_value('userinfo')) {
            $responses = new backup_nested_element('responses');
            $response = new backup_nested_element('response', ['id'], ['userid', 'response', 'complete']);

            $devpage->add_child($responses);
            $responses->add_child($response);

            $response->set_source_table('devpage_responses', ['devpageid' => backup::VAR_PARENTID], 'id ASC');

            // map response userid to user table
            $response->annotate_ids('user', 'userid');
        }

        return $this->prepare_activity_structure($devpage);
    }
}
