<?php

/**
 * Define the restore steps
 * used by restore_devpage_activity_task (./restore_devpage_activity_task.class.php)
 */

class restore_devpage_activity_structure_step extends restore_activity_structure_step {

    // define the restore tree structure: devpage > devpage_responses
    protected function define_structure() {
        $paths = [new restore_path_element('devpage', '/activity/devpage')];
        if ($this->get_setting_value('userinfo')) {
            // only if userdata is included in restore
            $paths[] = new restore_path_element('devpage_response', '/activity/devpage/responses/response');
        }

        return $this->prepare_activity_structure($paths);
    }


    // restore devpage table entry
    protected function process_devpage($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();

        $newitemid = $DB->insert_record('devpage', $data);
        $this->apply_activity_instance($newitemid); // need this
    }


    // restore devpage_responses table entries
    protected function process_devpage_response($data) {
        global $DB;

        $data = (object)$data;

        $data->devpageid = $this->get_new_parentid('devpage');
        $data->userid = $this->get_mappingid('user', $data->userid);

        $newitemid = $DB->insert_record('devpage_responses', $data);
        // no need to call apply_activity_instance mapping as nothing depends on it (child paths, file areas nor links decoder)
    }


    // add devpage related files (can't add files in content, only intro)
    protected function after_execute() {
        $this->add_related_files('mod_devpage', 'intro', null);
    }
}
