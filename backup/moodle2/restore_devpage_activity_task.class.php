<?php


/**
 * Restore individual devpage instances
 * by calling the backup steps (./backup_devpage_stepslib.php)
 * and decoding devpage urls.
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/devpage/backup/moodle2/restore_devpage_stepslib.php');

class restore_devpage_activity_task extends restore_activity_task {

    // no settings, but still need this
    protected function define_my_settings() {}


    // restore steps to restore the instance data from devpage.xml backup file
    protected function define_my_steps() {
        $this->add_step(new restore_devpage_activity_structure_step('devpage_structure', 'devpage.xml'));
    }


    // state which devpage fields require url decoding
    static public function define_decode_contents() {
        return [new restore_decode_content('devpage', ['intro', 'content'], 'devpage')];
    }


    // correct devpage urls in restored course
    static public function define_decode_rules() {
        return [
          new restore_decode_rule('DEVPAGEVIEWBYID', '/mod/devpage/view.php?id=$1', 'course_module'),
          new restore_decode_rule('DEVPAGEINDEX', '/mod/devpage/index.php?id=$1', 'course')
        ];
    }


    // the restore log rules for restoring devpage logs
    static public function define_restore_log_rules() {
        return [
          new restore_log_rule('devpage', 'add', 'view.php?id={course_module}', '{devpage}'),
          new restore_log_rule('devpage', 'update', 'view.php?id={course_module}', '{devpage}'),
          new restore_log_rule('devpage', 'view', 'view.php?id={course_module}', '{devpage}')
        ];
    }


    // the restore log rules for restoring course logs
    static public function define_restore_log_rules_for_course() {
        return [new restore_log_rule('devpage', 'view all', 'index.php?id={course}', null)];
    }
}
