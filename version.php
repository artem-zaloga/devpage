<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2023071800;
$plugin->requires  = 2019051100;
$plugin->component = 'mod_devpage';
