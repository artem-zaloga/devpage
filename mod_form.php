<?php

/**
 * The form that is shown when creating/editing a devpage instance.
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/course/moodleform_mod.php');
require_once($CFG->libdir.'/filelib.php');

class mod_devpage_mod_form extends moodleform_mod {
    public function definition() {
        global $PAGE, $CFG;

        $mform = $this->_form;
        $config = get_config('devpage');

        // make General section
        $mform->addElement('header', 'general', get_string('general', 'form'));

        // devpage instance name
        $mform->addElement('text', 'name', get_string('name'), array('size'=>'48'));
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        // add the description form elements; this is mandatory or moodle yells at you
        $this->standard_intro_elements();

        // make Content section
        $mform->addElement('header', 'contentsection', get_string('contentheader', 'devpage'));

        // devpage content
        $mform->addElement('textarea', 'content', get_string('content', 'devpage'), 'id="content-editor" rows="5" style="width:100%"');
        $mform->addRule('content', get_string('required'), 'required', null, 'client');

        // set devpage use docs as default content
        $sample = <<<SAMPLE
<!--
mod_devpage use

- Press Shift+DoubleLeftClick to open full screen editing mode (after you focus on the content editor; Esc to exit)
- Press Shift+Space to save the editor content without refreshing the page
- content in style tags with an attribute of 'type="scss"' will be parsed as scss
- content in script tags with an attribute of 'type="module"' will be added to requirejs' stack
- 'mod_devpage/api' js module can be used to set/get/list user responses and get/list course users

See example below for details.
-->

<style type="scss">
  #stuff {
    .purple {
      color: rebeccapurple;
    }
  }
</style>

<!-- this is a vue template; see script below -->
<div v-cloak id="stuff">
  <p>Hello {{ user }}!</p>
  <form @submit.prevent="saveText">
    <label>Enter something you like:<input v-model="newResponse"></label>
    <button type="submit">Save</button>
  </form>
  <p class="purple">Your responses: {{ oldResponses }}</p>
  <p>All users: {{ allUsers }}!</p>
  <p>All users' past responses: {{ allResponses }}</p>
</div>

<script type="module">
function createActivity(api, Vue) {
  new Vue({
    el: '#stuff',
    data: () => ({
      user: {},
      allUsers: {},
      newResponse: '',
      oldResponses: [],
      allResponses: {}
    }),
    // below is a description of the devpage api methods, with sample use
    // all api methods are Promises
    // options object is optional and (should) have sensible defaults if left out
    // * indicates that access is restricted to users who have 'mod/devpage:datafullaccess' access (by default: teacher, editingteacher, manager, admin)
    created() {
      // getUser()
      // gets the current user in the context of this course
      // returns { firstName, lastName, roles }

      api.getUser()
        .then(r => { this.user = r })
        .catch(e => { console.log(e) })

      // *listUsers()
      // lists all users in the context of this course
      // returns { userId: { firstName, lastName, roles }, ... } (roles are site roles, not course aliases)

      api.listUsers()
        .then(r => { this.allUsers = r})
        .catch(e => { console.log(e) })

      // getResponse({ url })
      // gets current user's response
      // 'url' defaults to current page url: what is the full url of the devpage activity?
      // returns data that was previously set, or null

      api.getResponse()
        .then(r => { this.oldResponses = r || [] })
        .catch(e => { console.log(e) })

      // *listResponses({ url })
      // lists all users' responses
      // 'url' defaults to current page url: what is the full url of the devpage activity?
      // returns { userId: userResponse, ... }

      api.listResponses()
        .then(r => { this.allResponses = r })
        .catch(e => { console.log(e) })
    },
    methods: {
      saveText() {
        // setResponse(data, { *userId, complete })
        // sets user's response for the current devpage instance
        // data must be JSON.stringify()-able js data
        // 'userId' defaults to current user id: which user should the response be set for?
        // 'complete' defaults to false: is the activity complete? (only applicable if custom completion options are set for activity; once set to true, only course reset can change completion status)

        api.setResponse([...this.oldResponses, this.newResponse], { complete: this.newResponse == 'cats' })
          .then(() => { 
            this.oldResponses = this.oldResponses.concat([this.newResponse])
            this.newResponse = ''
           })
          .catch(e => { console.log(e) })
      }
    }
  })
}

// require local devpage api and cdn vue and then create the activity
require(
  ['mod_devpage/api', 'https://cdn.jsdelivr.net/npm/vue@2.6.14'], 
  function(devApi, cdnVue) { createActivity(devApi, window.Vue || cdnVue) }
)
</script>
SAMPLE;
        $mform->setDefault('content', $sample);

        // add all the standard activity form stuff
        $this->standard_coursemodule_elements();
        $this->add_action_buttons();
        $mform->addElement('hidden', 'revision');
        $mform->setType('revision', PARAM_INT);
        $mform->setDefault('revision', 1);

        // add editor style and init the editor js
        $PAGE->requires->css('/mod/devpage/js/codemirror/lib/codemirror.css');
        $PAGE->requires->css('/mod/devpage/js/codemirror/addon/fold/foldgutter.css');
        $PAGE->requires->css('/mod/devpage/js/codemirror/addon/display/fullscreen.css');
        $PAGE->requires->css('/mod/devpage/js/codemirror/theme/nord.css');
        $PAGE->requires->js_call_amd('mod_devpage/editor', 'init', ['content-editor', 'nord']);
    }


    // create custom completion form
    public function add_completion_rules() {
        $this->_form->addElement('advcheckbox', 'customcompletion', '', get_string('customcompletion', 'devpage'));
        return ['customcompletion'];
    }

    // show customcompletion in completion form
    public function completion_rule_enabled($data) {
        return $data['customcompletion'];
    }
}
