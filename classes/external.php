<?php

/**
 * The external functions
 * which are exposed by the webservices (../db/services.php)
 * and called by the JS api (../amd/src/api.js).
 * TODO: add db write failure checks
 */

require_once($CFG->libdir . "/externallib.php");
require_once($CFG->dirroot.'/lib/completionlib.php');

class mod_devpage_external extends external_api {
    /* ------------  save devpage content  ------------ */

    public static function set_devpage_content_parameters() {
        return new external_function_parameters(
            [
                'courseid' => new external_value(PARAM_RAW, 'The courseid.'),
                'cmid' => new external_value(PARAM_INT, 'The cmid.'),
                'content' => new external_value(PARAM_RAW, 'The devpage content.'),
            ]
        );
    }


    public static function set_devpage_content($courseid, $cmid, $content) {
        global $DB, $USER;

        // get cm and devpage from cmid or bail
        $cm = $DB->get_record('course_modules', ['id' => $cmid]);
        $devpage = $DB->get_record('devpage', ['id' => $cm->instance]);
        if (!$devpage) {
            throw new Exception("Can't find devpage {$cm->instance}");
        }

        // update record
        $DB->update_record('devpage', array_merge((array)$devpage, ['content' => $content]));
        return true;
    }


    public static function set_devpage_content_returns() {
        return new external_value(PARAM_BOOL, 'Success or failure of write.');
    }

    /* ------------  get devpage response ------------ */

    // input parameter type check
    public static function get_devpage_response_parameters() {
        return new external_function_parameters(
            [
              'cmid' => new external_value(PARAM_RAW, 'The cmid.'),
              'all' => new external_value(PARAM_BOOL, 'Get all responses?'),
            ]
        );
    }


    // the actual webservice function
    public static function get_devpage_response($cmid, $all) {
        global $DB, $USER;

        // get devpage from cmid or bail
        $cminstance = $DB->get_field('course_modules', 'instance',['id' => $cmid]);
        $devpageid = $DB->get_field('devpage', 'id', ['id' => $cminstance]);
        if (!$devpageid) {
            throw new Exception("Can't find devpage {$cm->instance}");
        }

        // return array if for all or just individual object if for one

        if ($all) {
          return has_capability('mod/devpage:datafullaccess', context_module::instance($cmid))
            ? json_encode($DB->get_records_select_menu('devpage_responses', 'devpageid = :devpageid', ['devpageid' => $devpageid], '', 'userid, response'))
            : json_encode([]);
        } else {
          $response = $DB->get_field('devpage_responses', 'response', ['userid' => $USER->id, 'devpageid' => $devpageid]);
          return $response ? $response : json_encode(null);
        }
    }


    // output type check; data types can vary, so just json_encode output and always expect a string
    public static function get_devpage_response_returns() {
        return new external_value(PARAM_RAW, 'Return value.');
    }


    /* ------------ set devpage response ------------ */

    public static function set_devpage_response_parameters() {
        return new external_function_parameters(
            [
                'cmid' => new external_value(PARAM_INT, 'The cmid.'),
                'userid' => new external_value(PARAM_INT, 'The userid, or 0 if current user.'),
                'response' => new external_value(PARAM_RAW, 'The JSON.stringify\'d user response for this devpage.'),
                'complete' => new external_value(PARAM_BOOL, 'User has completed this devpage.')
            ]
        );
    }


    public static function set_devpage_response($cmid, $userid, $response, $complete) {
        global $DB, $USER;

        // set user id or default to current user
        $uid = $userid != 0 && has_capability('mod/devpage:datafullaccess', context_module::instance($cmid)) ? $userid : $USER->id;

        // get cm and devpage from cmid or bail
        $cm = $DB->get_record('course_modules', ['id' => $cmid]);
        $devpage = $DB->get_record('devpage', ['id' => $cm->instance]);
        if (!$devpage) {
            throw new Exception("Can't find devpage {$cm->instance}");
        }

        // update/create devpage response record for user and return updated response
        $record = $DB->get_record('devpage_responses', ['devpageid' => $devpage->id,'userid' => $uid]);
        if ($record) {
            $DB->update_record('devpage_responses', array_merge((array)$record, ['response' => $response, 'complete' => $complete || $record->complete ? 1 : 0]));
        } else {
            $DB->insert_record('devpage_responses', [ 'devpageid' => $devpage->id, 'userid' => $uid, 'response' => $response, 'complete' => $complete ? 1 : 0]);
        }

        // notify completion system if customcompletion is set on instance and user's completion status changed from 0 to 1
        if ($devpage->customcompletion && $complete && !$record->complete) {
            $completion = new completion_info($DB->get_record('course', ['id' => $devpage->course]));
            $completion->update_state($cm, COMPLETION_COMPLETE);
        }

        return $response;
    }


    public static function set_devpage_response_returns() {
        return new external_value(PARAM_RAW, 'Return value.');
    }


    /* ------------ get user in-course data ------------ */

    public static function get_user_data_parameters() {
        return new external_function_parameters(
            [
                'courseid' => new external_value(PARAM_RAW, 'The courseid.'),
                'cmid' => new external_value(PARAM_INT, 'The cmid.'),
                'all' => new external_value(PARAM_BOOL, 'Get all course users?')
            ]
        );
    }


    public static function get_user_data($courseid, $cmid, $all) {
        global $USER;

        // get course context
        $context = context_course::instance($courseid);

        // make user object; course roles are system roles, not course aliases
        function make_user($context, $user) {
          return [
            'firstName' => $user->firstname,
            'lastName' => $user->lastname,
            'roles' => array_values(array_map(
                  function($role) { return $role->shortname; },
                  get_user_roles($context, $user->id, false)
                ))
          ];
        }

        if ($all) {
          return has_capability('mod/devpage:datafullaccess', context_module::instance($cmid))
            ? json_encode(array_reduce(
                get_enrolled_users($context),
                function($userMap, $user) use (&$context) {
                  $userMap[$user->id] = make_user($context, $user);
                  return $userMap;
                },
                []
              ))
             : json_encode([]);
        } else {
          return json_encode(make_user($context, $USER));
        }
    }


    public static function get_user_data_returns()
    {
        return new external_value(PARAM_RAW, 'Return value.');
    }
}
