<?php

/**
 * The devpage course module viewed event
 * called in devpage_view function (../../lib.php).
 */

namespace mod_devpage\event;
defined('MOODLE_INTERNAL') || die();

class course_module_viewed extends \core\event\course_module_viewed {

    // create base properties of event; 'context' and 'objectid' will be added in when class is instantiated
    protected function init() {
        $this->data['crud'] = 'r';
        $this->data['edulevel'] = self::LEVEL_PARTICIPATING;
        $this->data['objecttable'] = 'devpage';
    }

    // point to correct table when event is restored
    public static function get_objectid_mapping() {
        return ['db' => 'devpage', 'restore' => 'devpage'];
    }
}
