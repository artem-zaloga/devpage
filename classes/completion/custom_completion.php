<?php
/**
 * New way to do custom completion.
 */

declare(strict_types=1);
namespace mod_devpage\completion;
use core_completion\activity_custom_completion;

class custom_completion extends activity_custom_completion {

    // Get the custom completion state.
    public function get_state(string $rule): int {
        global $DB;

        $this->validate_rule($rule);

        $status = $DB->get_field('devpage_responses', 'complete', ['devpageid' => $this->cm->instance, 'userid' => $this->userid]) == 1;
        return $status ? COMPLETION_COMPLETE : COMPLETION_INCOMPLETE;
    }

    // Names of custom completion rules.
    public static function get_defined_custom_rules(): array {
        return ['customcompletion'];
    }

    // Descriptions of custom completion rules.
    public function get_custom_rule_descriptions(): array {
        return [
            'customcompletion' => get_string('customcompletiondetail', 'devpage')
        ];
    }

    // All completion rules, in the order.
    public function get_sort_order(): array {
        return ['completionview', 'customcompletion'];
    }
}
