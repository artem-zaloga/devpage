<?php

/**
 * The main file that contains php code that will be shoved into moodle directly.
 */

defined('MOODLE_INTERNAL') || die;


/**
 * List of features supported in Devpage module
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed true if module supports feature, false if not, null if doesn't know
 */
function devpage_supports($feature) {
    switch ($feature) {
        case FEATURE_GROUPS:                  return false;
        case FEATURE_GROUPINGS:               return false;
        case FEATURE_MOD_INTRO:               return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS: return true;
        case FEATURE_COMPLETION_HAS_RULES:    return true;
        case FEATURE_GRADE_HAS_GRADE:         return false;
        case FEATURE_GRADE_OUTCOMES:          return false;
        case FEATURE_BACKUP_MOODLE2:          return true;
        case FEATURE_SHOW_DESCRIPTION:        return true;
        default: return null;
    }
}


/* ------------ Instance CRUD ------------ */


/**
 * Add devpage instance.
 * @param stdClass $data
 * @param mod_devpage_mod_form $mform
 * @return int new devpage instance id
 */
function devpage_add_instance($data, $mform) {
    global $CFG, $DB;
    require_once("$CFG->libdir/resourcelib.php");

    $data->timemodified = time();
    $data->id = $DB->insert_record('devpage', $data);

    return $data->id;
}


/**
 * Update devpage instance.
 * @param stdClass $data
 * @param mod_devpage_mod_form $mform
 * @return bool true
 */
function devpage_update_instance($data, $mform) {
    global $CFG, $DB;
    require_once("$CFG->libdir/resourcelib.php");

    $data->timemodified = time();
    $data->id = $data->instance;
    $data->revision++;

    $DB->update_record('devpage', $data);

    return true;
}


/**
 * Delete devpage instance and associated devpage responses.
 * @param int $id
 * @return bool true
 */
function devpage_delete_instance($id) {
    global $DB;

    if (!$DB->delete_records("devpage_responses", ["devpageid"=>"$id"])) {
        return false;
    }
    if (!$DB->delete_records("devpage", ["id"=>"$id"])) {
        return false;
    }
    return true;
}


/* ------------ Events and Completion ------------ */


/**
 * Trigger the course_module_viewed event and mark the activity completed (if required).
 *
 * @param  stdClass $devpage devpage object
 * @param  stdClass $course  course object
 * @param  stdClass $cm      course module object
 * @param  stdClass $context context object
 * @since Moodle 2.9
 */
function devpage_view($devpage, $course, $cm, $context) {

    // trigger view event
    $event = \mod_devpage\event\course_module_viewed::create(['context' => $context, 'objectid' => $devpage->id]);
    $event->add_record_snapshot('course_modules', $cm);
    $event->add_record_snapshot('course', $course);
    $event->add_record_snapshot('devpage', $devpage);
    $event->trigger();

    // set module view completion (if necessary)
    $completion = new completion_info($course);
    $completion->set_module_viewed($cm);
}


/**
 * Get completion state of individual devpage instance; called just about whenever completion_info is instantiated.
 * NOTE: deprecated in favour of ./classes/completion/custom_completion
 * @param object $course Course
 * @param object $cm Course-module
 * @param int $userid User ID
 * @param bool $type Type of comparison (or/and; can be used as return value if no conditions)
 * @return bool True if completed, false if not. (If no conditions, then return value depends on comparison type)
 */
function devpage_get_completion_state($course, $cm, $userid, $type) {
    global $CFG,$DB;

    // get devpage instance or bail
    $devpage = $DB->get_record('devpage', ['id' => $cm->instance]);
    if (!$devpage) {
        throw new Exception("Can't find devpage {$cm->instance}");
    }

    if ($devpage->customcompletion) {
        return $DB->get_field('devpage_responses', 'complete', ['devpageid' => $devpage->id, 'userid' => $userid]) == 1;
    } else {
        return $type; // Default return value
    }
}


/**
 * Extra activity information for printing in a course listing.
 *
 * @param stdClass $cm The cm object (record).
 * @return cached_cm_info Course info object
 */
function devpage_get_coursemodule_info($cm) {
    global $DB;

    $devpage = $DB->get_record('devpage', ['id'=>$cm->instance], '*', MUST_EXIST);
    if (!$devpage) {
        return false;
    }

    $result = new cached_cm_info();
    $result->name = $devpage->name;

    // Intro
    if ($cm->showdescription) {
        $result->content = format_module_intro('devpage', $devpage, $cm->id, false);
    }

    // Custom completion
    if ($cm->completion == COMPLETION_TRACKING_AUTOMATIC) {
        $result->customdata['customcompletionrules']['customcompletion'] = $devpage->customcompletion;
    }

    return $result;
}


/**
 * String describing the active completion custom rules.
 *
 * @param cm_info|stdClass $cm object with fields ->completion and ->customdata['customcompletionrules']
 * @return array $descriptions rule descriptions array.
 */
function mod_devpage_get_completion_active_rule_descriptions($cm) {
    if (empty($cm->customdata['customcompletionrules']) || $cm->completion != COMPLETION_TRACKING_AUTOMATIC) {
        return [];
    }

    $descriptions = [];
    if (!empty($cm->customdata['customcompletionrules']['customcompletion'])) {
        $descriptions[] = get_string('customcompletion', 'devpage');
    }
    return $descriptions;
}

/* ------------ Course Reset ------------ */


/**
 * Create devpages reset data form; called by course/reset.php
 *
 * @param $mform form passed by reference
 */
function devpage_reset_course_form_definition(&$mform) {
    $mform->addElement('header', 'devpageheader', get_string('modulenameplural', 'devpage'));
    $mform->addElement('checkbox', 'reset_devpage_responses', get_string('resetresponses', 'devpage'));
}


/**
 * Course reset devpage form defaults.
 * @return array
 */
function devpage_reset_course_form_defaults($course) {
    return ['reset_devpage_responses'=>1];
}


/**
 * This function will remove all devpage_responses from the specified course;
 * called by the reset_course_userdata function in moodlelib.
 *
 * @param $data the data submitted from the reset course form.
 * @return array status array
 */
function devpage_reset_userdata($data) {
    global $DB;

    $status = [];
    if (!empty($data->reset_devpage_responses)) {
        $responseIDs = $DB->get_fieldset_select('devpage', 'id', 'course = :course', ['course' => $data->courseid]);
        $DB->delete_records_list('devpage_responses', 'devpageid', $responseIDs);

        $status[] = [
          'component' => get_string('modulenameplural', 'devpage'),
          'item' => get_string('resetresponses', 'devpage'),
          'error' => false
          ];
    }

    return $status;
}
