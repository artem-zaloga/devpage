<?php

/**
 * The webservices
 * that connect the external functions (../classes/external.php)
 * to the JS api (../amd/src/api.js).
 */

 $functions = [
   'mod_devpage_set_devpage_content' => [
       'classname' => 'mod_devpage_external',
       'methodname' => 'set_devpage_content',
       'description' => 'Set devpage content.',
       'type' => 'write',
       'ajax' => true,
       'loginrequired' => true,
   ],
     'mod_devpage_get_devpage_response' => [
         'classname' => 'mod_devpage_external',
         'methodname' => 'get_devpage_response',
         'description' => 'Get devpage user response.',
         'type' => 'read',
         'ajax' => true,
         'loginrequired' => true,
     ],
     'mod_devpage_set_devpage_response' => [
         'classname' => 'mod_devpage_external',
         'methodname' => 'set_devpage_response',
         'description' => 'Set devpage user response.',
         'type' => 'write',
         'ajax' => true,
         'loginrequired' => true,
     ],
     'mod_devpage_get_user_data' => [
         'classname' => 'mod_devpage_external',
         'methodname' => 'get_user_data',
         'description' => 'Get user in-course data.',
         'type' => 'read',
         'ajax' => true,
         'loginrequired' => true,
     ]
];
